//
//  Decodable + Extension.swift
//  ForceAndRecommendUpdate
//
//  Created by 季紅 on 2023/9/11.
//

import Foundation

extension Decodable {
    static func decode(from jsonData: Data) -> Self? {
        do {
            return try JSONDecoder().decode(self, from: jsonData)
        } catch {
            print("解码错误: \(error)")
            return nil
        }
    }
}
