//
//  AppVersionUpdateHandler.swift
//  ForceAndRecommendUpdate
//
//  Created by 季紅 on 2023/9/15.
//

import Foundation

class AppVersionUpdateHandler: UpdateTypeHandler {
    func handleUpdate(type: UpdateType) {
        switch type {
        case .none:
            break
        case .recommend:
            print("建議更新")
        case .force:
            print("強制更新")
        }
    }
}
