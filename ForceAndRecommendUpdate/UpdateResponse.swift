//
//  UpdateResponse.swift
//  ForceAndRecommendUpdate
//
//  Created by 季紅 on 2023/9/11.
//

import Foundation

struct UpdateResponse: Codable {
    let forceUpdateVersion, recommendUpdateVersion: String
    
    enum CodingKeys: String, CodingKey {
        case forceUpdateVersion = "force_update_version"
        case recommendUpdateVersion = "recommend_update_version"
    }
}
