//
//  UpdateType.swift
//  ForceAndRecommendUpdate
//
//  Created by 季紅 on 2023/9/11.
//

import Foundation

enum UpdateType: Equatable {
    case none
    case recommend
    case force
    
    static func checkForUpdates(currentVersion: String, forceUpdateVersion: String, recommendUpdateVersion: String) -> UpdateType {
        let currentVersionComparison = compareVersions(currentVersion, forceUpdateVersion)
        let recommendUpdateComparison = compareVersions(currentVersion, recommendUpdateVersion)
        
        if currentVersionComparison == .orderedSame && recommendUpdateComparison == .orderedAscending {
            return .recommend
        } else if currentVersionComparison == .orderedAscending || recommendUpdateComparison == .orderedAscending {
            return .force
        } else {
            return .none
        }
    }
    
    private static func compareVersions(_ version1: String, _ version2: String) -> ComparisonResult {
        return version1.compare(version2, options: .numeric)
    }
}
