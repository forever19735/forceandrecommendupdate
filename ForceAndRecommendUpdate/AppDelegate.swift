//
//  AppDelegate.swift
//  ForceAndRecommendUpdate
//
//  Created by 季紅 on 2023/9/11.
//

import UIKit
import Firebase
import FirebaseRemoteConfig

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var remoteConfig: RemoteConfig!
    
    private lazy var appUpdateManager: AppUpdateManager = {
        let appVersionUpdateHandler = AppVersionUpdateHandler()
        let appUpdateManager = AppUpdateManager(remoteConfig: remoteConfig)
        appUpdateManager.registerUpdateTypeHandler(handler: appVersionUpdateHandler)

        return appUpdateManager
    }()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        configureFirebase()
        setupRemoteConfig()
                
        appUpdateManager.checkAndHandleAppUpdate()

        listenForConfigUpdates()
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        appUpdateManager.checkAndHandleAppUpdate()
    }


}

extension AppDelegate {
    private func configureFirebase() {
        //MARK: Firebase
        let filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")
       
        guard let fileopts = FirebaseOptions(contentsOfFile: filePath!) else {
            assert(false, "Couldn't load config file")
            return
        }
        FirebaseApp.configure(options: fileopts)
    }
    
    private func setupRemoteConfig() {
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = 0
        
        remoteConfig = RemoteConfig.remoteConfig()
        remoteConfig.configSettings = settings
    }
    
    private func listenForConfigUpdates() {
        remoteConfig.addOnConfigUpdateListener { [weak self] configUpdate, error in
            guard let self = self else { return }
            
            guard let configUpdate = configUpdate, error == nil else {
                print("Error listening for config updates: \(error)")
                return
            }
                        
            self.remoteConfig.activate { [weak self] changed, error in
                guard let self = self else { return }
                
                for updateKey in configUpdate.updatedKeys {
                    switch updateKey {
                    case self.appUpdateManager.updateVersion:
                        appUpdateManager.handleUpdateKey()
                    default:
                        break
                    }
                }
            }
        }
    }
}
