//
//  AppUpdateManager.swift
//  ForceAndRecommendUpdate
//
//  Created by 季紅 on 2023/9/11.
//

import Foundation
import FirebaseRemoteConfig

protocol UpdateTypeHandler {
    func handleUpdate(type: UpdateType)
}

class AppUpdateManager {
    let updateVersion = "update_version"

    private var currentVersion: String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String ?? ""
    }
    
    private let remoteConfig: RemoteConfig
    
    init(remoteConfig: RemoteConfig) {
        self.remoteConfig = remoteConfig
    }
    
    private var updateTypeHandler: UpdateTypeHandler?
    
    func registerUpdateTypeHandler(handler: UpdateTypeHandler) {
        self.updateTypeHandler = handler
    }
}

extension AppUpdateManager {
    private func getUpdateType() -> UpdateType {
        let jsonData = remoteConfig[updateVersion].dataValue
        guard let updateResponse = UpdateResponse.decode(from: jsonData) else {
            return .none
        }
        
        let forceUpdateVersion = updateResponse.forceUpdateVersion
        let recommendUpdateVersion = updateResponse.recommendUpdateVersion
        
        let type = UpdateType.checkForUpdates(currentVersion: currentVersion,
                                              forceUpdateVersion: forceUpdateVersion,
                                              recommendUpdateVersion: recommendUpdateVersion)
        return type
    }
}

extension AppUpdateManager {
    func checkAndHandleAppUpdate() {
        remoteConfig.fetch() { [weak self] (status, error) in
            guard let self = self else { return }
            guard status == .success, error == nil else {
                print("錯誤:\(error)")
                return
            }
            remoteConfig.activate()
            self.handleUpdateKey()
        }
    }
    
    func handleUpdateKey() {
        let type = getUpdateType()
        self.updateTypeHandler?.handleUpdate(type: type)
    }
}
