//
//  ForceAndRecommendUpdateTests.swift
//  ForceAndRecommendUpdateTests
//
//  Created by 季紅 on 2023/9/11.
//

import XCTest
import FirebaseRemoteConfig
@testable import ForceAndRecommendUpdate

class AppUpdateManagerTests: XCTestCase {
    func testCheckForUpdatesRecommendUpdate() {
        // 假設目前版本號是 "2.0"，強制更新的版本號是 "2.0"，推薦更新的版本號是 "3.0"
        let updateType = UpdateType.checkForUpdates(currentVersion: "2.0", forceUpdateVersion: "2.0", recommendUpdateVersion: "3.0")
        
        XCTAssertEqual(updateType, .recommend, "建議更新")
    }
    
    func testCheckForUpdatesForceUpdate() {
        // 假設目前版本號是 "1.0"，強制更新的版本號是 "2.0"，推薦更新的版本號是 "3.0"
        let updateType = UpdateType.checkForUpdates(currentVersion: "1.0", forceUpdateVersion: "2.0", recommendUpdateVersion: "3.0")
        
        XCTAssertEqual(updateType, .force, "強制更新")
    }
    
    func testCheckForUpdatesForceUpdate2() {
        // 假設目前版本號是 "1.0"，強制更新的版本號是 "3.0"，推薦更新的版本號是 "2.0"
        let updateType = UpdateType.checkForUpdates(currentVersion: "1.0", forceUpdateVersion: "3.0", recommendUpdateVersion: "2.0")
        
        XCTAssertEqual(updateType, .force, "強制更新")
    }
    
    func testCheckForUpdatesNoUpdate() {
        // 假設目前版本號是 "1.0"，強制更新的版本號是 "1.0"，推薦更新的版本號是 "1.0"
        let updateType = UpdateType.checkForUpdates(currentVersion: "1.0", forceUpdateVersion: "1.0", recommendUpdateVersion: "1.0")
        
        XCTAssertEqual(updateType, .none, "不更新")
    }
}
